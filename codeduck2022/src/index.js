import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import { 
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import { Provider } from 'react-redux'
import store from './store'

import ProtectedRoute from './components/elements/ProtectedRoute';
import Layout from './components/pages/Layout';
import Login from './components/pages/Login';
import Logout from './components/pages/Logout';
import AdminPanel from './components/pages/AdminPanel';
import Home from './components/pages/Home';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={ store }>
      <BrowserRouter>
        <Routes>
          <Route element={ <ProtectedRoute path={ 'login' } logged={ localStorage.getItem('isLogged') } /> } >
              <Route path="/login" element={ <Login /> } />
          </Route>
          <Route element={ <ProtectedRoute path={ 'admin-panel' } logged={ localStorage.getItem('isLogged') } /> } >
              <Route path="/admin-panel" element={ <AdminPanel />} />
          </Route>

          <Route path="/" element={ <Layout /> } user={ localStorage.getItem('userLogged') }>
            <Route exact element={ <Home /> }  />
            <Route path="logout" element={ <Logout /> } />

            <Route path="*" element={<p>There's nothing here: 404!</p>} />
          </Route>
        </Routes>
      </BrowserRouter>
      </ Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
