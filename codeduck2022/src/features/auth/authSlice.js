import { createSlice } from '@reduxjs/toolkit'

export const authSlice = createSlice({
  name: 'auth',
  initialState: {},
  reducers: {
    signUp: (state, action) => {
      const login = action.payload.login
      const password = action.payload.password
        if (login.length > 0 && password.length > 0) {
          //axios + auth
          localStorage.setItem('isLogged', true)
          localStorage.setItem('userLogged', {
              'login': 'Justyna',
              'role': 'admin'
            })
        }
    },

    logout: () => {
      localStorage.clear();
    }
  }
});

export const { signUp, logout } = authSlice.actions
export default authSlice.reducer