import { React, useState} from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { signUp } from '../../features/auth/authSlice'

 function Login() {
  const navigate = useNavigate()
  const dispache = useDispatch()
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  function handleSubmit(event) {
    event.preventDefault();
    const forms = document.querySelectorAll('.needs-validation');
    Array.prototype.slice.call(forms).forEach((form) => {
      if (!form.checkValidity()) {
        event.stopPropagation();
      }
      form.classList.add('was-validated');
      dispache(signUp({login: login, password: password}));

      if (localStorage.getItem('isLogged')) {
          navigate('/admin-panel')
      }
    });
  }

  return (
    <form className="row g-3 needs-validation mt-4" noValidate>
      <div className="col-md-4">
        <div className="form-outline">
          <input type="text" 
          className="form-control" 
          id="validationCustom01" 
          value={ login } 
          onChange={ e => setLogin(e.target.value) }
          required 
          />
          <label htmlFor="validationCustom01" className="form-label">Login</label>
          <div className="valid-feedback">Looks good!</div>
          <div className="invalid-feedback">Please write login.</div>
        </div>
      </div>

      <div className="col-md-4">
        <div className="form-outline">
          <input 
          className="form-control" 
          id="pass-input" 
          type="password"
          value={ password } 
          onChange={ e => setPassword(e.target.value) } 
          required 
          />
          <label htmlFor="pass-input" className="form-label">Password</label>
          <div className="valid-feedback">Looks good!</div>
          <div className="invalid-feedback">Please write password.</div>
        </div>
      </div>

      <div className="col-12">
        <button className="btn btn-primary" type="submit" onClick={ e => handleSubmit(e) }>Submit form</button>
      </div>
    </form>
  );
}
export default Login;

