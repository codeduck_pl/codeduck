import { Outlet, } from "react-router-dom"
import HomeBar from '../elements/HomeBar'
import { MDBContainer } from 'mdb-react-ui-kit'
import 'mdb-react-ui-kit/dist/css/mdb.min.css'

function Layout({user}) {
  return (
      <MDBContainer fluid>
        <HomeBar user={ user }/>
        <MDBContainer>
          <Outlet />
        </MDBContainer>
      </MDBContainer>
  );
}
export default Layout
