import React from "react";
import AdminBar from "../elements/AdminBar";

function AdminPanel() {
    return (
      <main>
          <AdminBar />
          <h1>Centrum dowodzenia</h1>
      </main>
    );
}
export default AdminPanel