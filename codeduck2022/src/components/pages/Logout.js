import { Navigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { logout } from '../../features/auth/authSlice'

 function Logout() {
  const dispache = useDispatch()
  
  dispache(logout());
  return <Navigate to='/' replace />;
}
export default Logout;

