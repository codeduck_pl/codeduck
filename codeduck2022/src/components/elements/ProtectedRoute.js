import { Outlet, Navigate } from "react-router-dom"

function ProtectedRoute({
    logged,
    path,
    redirectPath = '/'
}) {
    switch (path) {
        case 'login': {
            if (logged) {
                return <Navigate to={ redirectPath } replace />;
            }
        } break;
        case 'admin-panel': {
           if (!logged) {
               return <Navigate to={ redirectPath } replace />;
           }
        } break;
    }
    return <Outlet />;
}
export default ProtectedRoute;
